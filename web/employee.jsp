<%-- 
    Document   : employee
    Created on : Jan 8, 2020, 8:58:44 AM
    Author     : Viet
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form action="EmployeeServlet" method="POST">
            Full Name: <input type="text" name="fullname" required/><br/>
            Birthday: <input type="text" name="birthday" placeholder="yyyy-MM-dd" required><br/>
            Address: <input type="text" name="address" required><br/>
            Position: <input type="text" name="position" required><br/>
            Department: <input type="text" name="department" required><br/>
            <input type="submit" value="Submit"/>
            <input type="reset" value="Reset"/>
        </form>
    </body>
</html>
